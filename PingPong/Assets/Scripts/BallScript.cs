﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallScript : MonoBehaviour
{
    [SerializeField] private float minBallSize = 1;
    [SerializeField] private float maxBallSize = 3;
    [SerializeField] private int minBallSpeed = 150;
    [SerializeField] private int maxBallSpeed = 300;
    private Transform _transform;
    private Rigidbody2D _rigidbody2D;
    private SpriteRenderer _renderer;
    private float _size;
    private int _speed;
    private Vector2 _direction;

    private void Awake()
    {
        _transform = transform;
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _renderer = GetComponent<SpriteRenderer>();
        Restart();
    }

    private void Restart()
    {
        _rigidbody2D.Sleep();
        _size = Random.Range(minBallSize, maxBallSize);
        _transform.position = new Vector3(0, 0);
        _transform.localScale = new Vector3(_size, _size, 1);
        _renderer.color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
        _direction = new Vector2(Random.Range(-1.5f, 1.5f), Random.Range(0, 2) == 0 ? -1 : 1);
        _speed = Random.Range(minBallSpeed, maxBallSpeed);
        _rigidbody2D.WakeUp();
        _rigidbody2D.AddForce(_direction * _speed);
    }

    private void OnBecameInvisible()
    {
        Restart();
    }
}