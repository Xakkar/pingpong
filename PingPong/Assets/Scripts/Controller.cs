﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    [SerializeField] private Transform _player1;
    [SerializeField] private Transform _player2;
    [SerializeField] private Transform _wallLeft;
    [SerializeField] private Transform _wallRight;
    [SerializeField] private float _playerSpeed = 1;
    private float _oldmousePosX;
    private float _movePosX;
    private bool isOnline;

    private void Awake()
    {
        float x = (float)Screen.width / Screen.height;
        _wallLeft.position = new Vector3(-5 * x, 0);
        _wallRight.position = new Vector3(5 * x, 0);
    }

    void Update()
    {
        _movePosX = 0;
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
#if UNITY_MOBILE
        if (Input.touchCount > 0)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Moved)
                _movePosX = Input.GetTouch(0).deltaPosition.x;
        }
#else
        if (Input.GetMouseButton(0))
        {
            _movePosX = Input.mousePosition.x - _oldmousePosX;
            _oldmousePosX = Input.mousePosition.x;
        }
#endif
        if (_movePosX > 0)
        {
            _player1.Translate(Vector2.right * _playerSpeed * Time.deltaTime);
            _player2.Translate(Vector2.right * _playerSpeed * Time.deltaTime);
        }
        else if (_movePosX < 0)
        {
            _player1.Translate(-Vector2.right * _playerSpeed * Time.deltaTime);
            _player2.Translate(-Vector2.right * _playerSpeed * Time.deltaTime);
        }
        else
        {
            _player1.Translate(Vector2.zero);
            _player2.Translate(Vector2.zero);
        }
    }
}